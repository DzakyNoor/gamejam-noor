extends Node

var LIVES = 5
var BASE_SPEED = 200
var SPEED_MULTIPLIER = 1
var GRAVITY = 800
var JUMP_SPEED = -300
var LEVEL = "res://scenes/level/level1.tscn"
var MAIN_MENU = "res://scenes/main/main.tscn"
var WIN = "res://scenes/main/win.tscn"
var PLAYER_ALIVE = false
var PLAYER_LOSE = false
var PLAYER_OBJECT
var MUSIC

var LEVEL_SCRIPT = load("res://scripts/level/level1.gd").new()
var PLAYER_SCRIPT = load("res://scripts/player/player_red.gd").new()
	
	
func PLAYER_HIT():
	LIVES -= 1
	if(LIVES == 0):
		PLAYER_ALIVE = false
	if(PLAYER_OBJECT.position.y<100):
		PLAYER_OBJECT.velocity.y = 100
		
	elif(PLAYER_OBJECT.position.y>500):
		PLAYER_OBJECT.velocity.y = -600
		PLAYER_OBJECT.sprite.rotation_degrees=-60