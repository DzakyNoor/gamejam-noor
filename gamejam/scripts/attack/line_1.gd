extends Area2D

onready var sprite = get_node("Sprite")
onready var collision = get_node("CollisionShape2D")

export (int) var delay = 0
export (bool) var on_screen = false

var vector = Vector2(0,-30)

func _process(delta):
	if(on_screen):
		delay+=1
	
	if(delay >= 90 && delay <= 100):
		sprite.translate(vector)
		collision.translate(vector)
	
func _on_VisibilityEnabler2D_viewport_entered(viewport):
	on_screen = true

func _on_line_body_entered(body):
	pass