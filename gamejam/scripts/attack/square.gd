extends Area2D

onready var sprite_1 = get_node("Sprite 1")
onready var sprite_2 = get_node("Sprite 2")
onready var sprite_3 = get_node("Sprite 3")
onready var coll_shape = get_node("CollisionShape2D")
onready var vis_en = get_node("VisibilityEnabler2D")

var spriteShiftLeft = Vector2(1,0)

export (bool) var on_screen = false
export (bool) var out_screen = false

func _process(delta):
	if(on_screen):
		sprite_1.rotation-=2*delta
		sprite_2.rotation-=4*delta
		sprite_3.rotation+=1*delta
		
		sprite_1.translate(spriteShiftLeft)
		sprite_2.translate(spriteShiftLeft)
		sprite_3.translate(spriteShiftLeft)
		coll_shape.translate(spriteShiftLeft)
		vis_en.translate(spriteShiftLeft)
	


func _on_square_body_entered(body):
	global.PLAYER_HIT()

func _on_VisibilityEnabler2D_viewport_entered(viewport):
	on_screen = true


func _on_VisibilityEnabler2D_viewport_exited(viewport):
	if(on_screen):
		on_screen = false
		
		
