extends Area2D

onready var anim_attack = get_node("anim")
export (bool) var on_screen = false
export (int) var delay = 0

var spriteLoc1 = Vector2(0,10)
var spriteLoc2 = Vector2(0,-10)

func _on_VisibilityEnabler2D_viewport_entered(viewport):
	anim_attack.play("",false)
	on_screen = true


func _on_attack_body_entered(body):
	global.PLAYER_HIT()

func _process(delta):
	if(on_screen):
		delay+=1
	
	if(delay == 125):
		anim_attack.translate(spriteLoc2)
	elif(delay == 130):
		anim_attack.translate(spriteLoc1)