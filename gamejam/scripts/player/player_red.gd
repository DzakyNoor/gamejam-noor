extends KinematicBody2D

onready var anim_smoke = get_node("smoke animation/AnimationPlayer")
onready var sprite = get_node("player_sprite_animated")
onready var anim_hit = get_node("player hit animation/AnimationPlayer")

const UP = Vector2(0,-1)

var velocity = Vector2()


var script_attack = preload("res://scripts/attack/attack_1.gd").new()

func _ready():
	jump()

func get_input():
	if Input.is_action_just_pressed('up'):
		jump()
		
		# Note
		# Wtf smokenya ngikutin sprite rotate toh


func _physics_process(delta):
	velocity.y += delta * global.GRAVITY
	if(global.PLAYER_ALIVE):
		get_input()
		
	velocity = move_and_slide(velocity, UP)
	if sprite.rotation_degrees < 30:
		sprite.rotation+=1*delta
		
	#if(script_attack._on_attack_body_entered(""

func jump():
	velocity.y = global.JUMP_SPEED
	sprite.rotation_degrees=-30
	anim_smoke.stop()			# oh cool ini bagus lmao - fungsi: buat reset animasi
	anim_smoke.play("smoke animation")


