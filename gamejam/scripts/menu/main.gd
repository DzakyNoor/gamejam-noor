extends MarginContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (int) var counter1 = 0
export (int) var counter2 = 0

onready var sprite = get_node("player/AnimatedSprite")

var spriteLoc1 = Vector2(0,10)
var spriteLoc2 = Vector2(0,-10)

var velocity = Vector2()

# Called when the node enters the scene tree for the first time.
func _process(delta):
	counter1+=1
	if(counter1==40):
		counter1 = 0
		if(counter2==1):
			counter2=0
		else:
			counter2=1
		transformSprite()
	get_input()

func transformSprite():
	if(counter2==1):
		sprite.translate(get_transform().basis_xform(spriteLoc2))
	elif(counter2==0):
		sprite.translate(get_transform().basis_xform(spriteLoc1))


func get_input():
	if Input.is_action_just_pressed('up'):
		get_tree().change_scene(str(global.LEVEL))