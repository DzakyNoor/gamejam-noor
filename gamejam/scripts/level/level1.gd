extends Node2D

onready var player_body = get_node("player/KinematicBody2D")

onready var music = get_node("music/music_streamer")

onready var lose_color = get_node("GUI/ColorRect")
onready var lose_text = get_node("GUI/lose text")


const right = Vector2(1,0)
const scale_stuff = Vector2()

export (int) var color_delay = 0

export (int) var player_positionY = 0

var player_script = load("res://scripts/player/player_red.gd").new()


export (int) var delay = 0

func _ready():
	lose_color.modulate = Color(255,0,0,0)
	lose_text.modulate = Color(0, 0, 0, 0)
	global.MUSIC = music
	global.MUSIC.play()
	
	global.LIVES = 5
	global.PLAYER_ALIVE = true
	global.PLAYER_LOSE = false
	
	#pass # Replace with function body.

func _process(delta):
	global.PLAYER_OBJECT = get_node("player/KinematicBody2D")
	
	if(!global.PLAYER_ALIVE):
		delay += 1
		
	if(delay < 50):
		player_body.velocity.x = global.BASE_SPEED * global.SPEED_MULTIPLIER
	else:
		if(player_body.velocity.x > 0):
			player_body.velocity.x -= 10
		else:
			color_delay+=1
		
		if(color_delay < 80):
			lose_screen(color_delay)
		else:
			global.PLAYER_LOSE = true
			get_input()
			
func lose_screen(color_delay):
	lose_color.modulate = Color(1,0.2,0.2,color_delay*0.01)
	lose_text.modulate = Color(1, 1, 1, color_delay*0.01)
	
func get_input():
	if (Input.is_action_just_pressed('up') && global.PLAYER_LOSE):
		get_tree().change_scene(str(global.LEVEL))


func _on_win_zone_body_entered(body):
	global.MUSIC.stop()
	get_tree().change_scene(str(global.WIN))
	