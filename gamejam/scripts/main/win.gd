extends Node2D

export (int) var counter = 0
export (int) var delay = 0

onready var player = get_node("player")

var spriteLoc1 = Vector2(0,10)
var spriteLoc2 = Vector2(0,-10)

func _process(delta):
	get_input()
	counter += 1
	if(counter == 30):
		counter = 0
		if(delay == 0):
			delay+=1
			player.translate(get_transform().basis_xform(spriteLoc2))
		else:
			delay = 0
			player.translate(get_transform().basis_xform(spriteLoc1))
			
			
func get_input():
	if Input.is_action_just_pressed('up'):
		get_tree().change_scene(global.MAIN_MENU)