extends Node

onready var anim = get_node("smoke animation/AnimationPlayer")

func _process(delta):
	get_input()
	
func get_input():
	if Input.is_action_just_pressed('up'):
		anim.play("smoke animation")