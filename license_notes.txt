Font:
	- https://www.dafont.com/ (referenced from https://onextrapixel.com/25-free-pixel-perfect-fonts-for-8-bit-designs/)
		- Fipps (by pheist)
		- V5 Extender (by vFive Digital)
		- 8 Bit Wonder (by Joiro Hatgaya)

Music:
	I dont own any music in this program
	
	Special thanks for:
	- MDK (Morgan David King) - Press Start (The Single free album release)
		- "free music usage for creative common"
		- https://support.google.com/youtube/forum/AAAAiuErobUQzgvWCkawjc/?hl=ro

	for grant music usage for creative common

	*list might be increased

Sprites:
	(Almost) all sprites are my drawing, the alien (or attack, the way i named the file) shapes are referenced from universal 8-bit alien shapes

Level:
	Referenced from main music above



NOTE: If I broke any copyright law, please contact me through email (dzaky.noor.hasyim@gmail.com)